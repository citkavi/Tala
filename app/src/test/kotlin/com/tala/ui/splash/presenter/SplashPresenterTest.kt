package com.tala.ui.splash.presenter

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.doAnswer
import com.nhaarman.mockito_kotlin.mock
import com.tala.base.AppConstants
import com.tala.base.api.EndPoints
import com.tala.base.helper.SpHelper
import com.tala.ui.splash.model.DeviceInfo
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by kavi on 03/12/17.
 */
class SplashPresenterTest {

    private val view: SplashView = mock()
    private val api: EndPoints = mock()
    private val spHelper: SpHelper = mock()
    private lateinit var presenter: SplashPresenter

    @Before
    fun setup() {
        presenter = SplashPresenter(api, spHelper)
        presenter.attachView(view)
    }


    @Test
    fun test_getDeviceInfo_should_callSuccess() {
        val mockedCall: Call<DeviceInfo> = mock()
        val mockedResponse: DeviceInfo = mock()

        Mockito.`when`(api.getDeviceInformation("http://ip-api.com/json")).thenReturn(mockedCall)

        doAnswer {
            val callBack: Callback<DeviceInfo> = it.getArgument(0)

            callBack.onResponse(mockedCall, Response.success(mockedResponse))
        }.`when`(mockedCall).enqueue(any())

        presenter.getDeviceInfo()

        Mockito.verify(spHelper).putDatatoSharedPref(mockedResponse.lat.toString(), String::class.java,AppConstants.LATITUDE)
        Mockito.verify(spHelper).putDatatoSharedPref(mockedResponse.lon.toString(), String::class.java,AppConstants.LONGITUDE)

        Mockito.verify(view).navigateToListActivity()

    }

    @Test
    fun test_getDeviceInfo_should_callError() {
        val mockCall: Call<DeviceInfo> = mock()
        val mockThrowable: Throwable = mock()

        Mockito.`when`(api.getDeviceInformation("http://ip-api.com/json")).thenReturn(mockCall)

        doAnswer {
            val callBack: Callback<DeviceInfo> = it.getArgument(0)

            callBack.onFailure(mockCall, mockThrowable)
        }.`when`(mockCall).enqueue(any())

        presenter.getDeviceInfo()

        Mockito.verify(view).onError()

    }


}