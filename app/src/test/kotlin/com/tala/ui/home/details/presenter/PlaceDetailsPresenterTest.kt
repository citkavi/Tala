package com.tala.ui.home.details.presenter

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.doAnswer
import com.nhaarman.mockito_kotlin.mock
import com.tala.base.AppConstants
import com.tala.base.api.EndPoints
import com.tala.base.helper.SpHelper
import com.tala.ui.home.details.model.VenueDetails
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import tala.com.tala_kavi.BuildConfig
import kotlin.test.assertEquals

/**
 * Created by kavi on 03/12/17.
 */

class PlaceDetailsPresenterTest {

    private val view: PlaceDetailView = mock()
    private val api: EndPoints = mock()
    private lateinit var presenter: PlaceDetailsPresenter
    private val spHelper: SpHelper = mock()

    @Before
    fun setup() {
        presenter = PlaceDetailsPresenter(api, spHelper)
        presenter.attachView(view)
    }


    @Test
    fun test_fetchLocationDetails_should_callSuccess() {
        val mockedCall: Call<VenueDetails> = mock()
        val mockedResponse: VenueDetails = mock()

        val hashMap = HashMap<String, Any>()
        hashMap.put("client_id", BuildConfig.CLIENT_ID)
        hashMap.put("client_secret", BuildConfig.CLIENT_SECRET)
        hashMap.put("v", BuildConfig.API_VERSION)


        Mockito.`when`(api.getPlacesDetails("1234",hashMap)).thenReturn(mockedCall)

        doAnswer {
            val callBack: Callback<VenueDetails> = it.getArgument(0)

            callBack.onResponse(mockedCall, Response.success(mockedResponse))
        }.`when`(mockedCall).enqueue(any())

        presenter.fetchLocationDetails("1234")


        Mockito.verify(view).populatePlaceDetails(mockedResponse.response)

    }

    @Test
    fun test_fetchLocationDetails_should_callError() {
        val mockCall: Call<VenueDetails> = mock()
        val mockThrowable: Throwable = mock()

        val hashMap = HashMap<String, Any>()
        hashMap.put("client_id", BuildConfig.CLIENT_ID)
        hashMap.put("client_secret", BuildConfig.CLIENT_SECRET)
        hashMap.put("v", BuildConfig.API_VERSION)

        Mockito.`when`(api.getPlacesDetails("1234",hashMap)).thenReturn(mockCall)

        doAnswer {
            val callBack: Callback<VenueDetails> = it.getArgument(0)

            callBack.onFailure(mockCall, mockThrowable)
        }.`when`(mockCall).enqueue(any())

        presenter.fetchLocationDetails("1234")

        Mockito.verify(view).onError()

    }

    @Test
    fun test_getLatLng_shouldReturnString(){
        Mockito.`when`(spHelper.getDataFromSharedPref(String::class.java, AppConstants.LATITUDE)).thenReturn("12.0")
        Mockito.`when`(spHelper.getDataFromSharedPref(String::class.java, AppConstants.LONGITUDE)).thenReturn("40.0")
        assertEquals("12.0,40.0", presenter.getLatLng())

    }

}