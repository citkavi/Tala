package com.tala.ui.home.presenter

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.doAnswer
import com.nhaarman.mockito_kotlin.mock
import com.tala.TestDataFactory
import com.tala.base.AppConstants
import com.tala.base.api.EndPoints
import com.tala.base.helper.SpHelper
import com.tala.ui.home.model.ExploredPlaces
import com.tala.ui.home.model.Item
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import tala.com.tala_kavi.BuildConfig

/**
 * Created by kavi on 03/12/17.
 */


class PlacesListPresenterTest {


    private val view: PlacesListView = mock()
    private val api: EndPoints = mock()
    private lateinit var presenter: PlacesListPresenter
    private val spHelper: SpHelper = mock()

    private lateinit var hashMap: HashMap<String, Any>


    @Before
    fun setup() {
        presenter = PlacesListPresenter(api, spHelper)
        presenter.attachView(view)

        hashMap = HashMap<String, Any>()
        hashMap.put("client_id", BuildConfig.CLIENT_ID)
        hashMap.put("client_secret", BuildConfig.CLIENT_SECRET)
        hashMap.put("v", BuildConfig.API_VERSION)
        hashMap.put("ll", "" + 12.0 + "," + 40.0)
        hashMap.put("venuePhotos", 1)

    }

    @Test
    fun test_fetchRecomendedLocations_should_callSuccess() {
        val mockedCall: Call<ExploredPlaces> = mock()
        val mockedResponse: ExploredPlaces = TestDataFactory.getTestResponse()


        Mockito.`when`(api.getExplorePlaces(hashMap)).thenReturn(mockedCall)
        Mockito.`when`(spHelper.getDataFromSharedPref(String::class.java, AppConstants.LATITUDE)).thenReturn("12.0")
        Mockito.`when`(spHelper.getDataFromSharedPref(String::class.java, AppConstants.LONGITUDE)).thenReturn("40.0")

        doAnswer {
            val callBack: Callback<ExploredPlaces> = it.getArgument(0)

            callBack.onResponse(mockedCall, Response.success(mockedResponse))
        }.`when`(mockedCall).enqueue(any())

        presenter.fetchRecomendedLocations()


        Mockito.verify(view).populateRecomendedVenues(mockedResponse.response.groups.get(0).items as MutableList<Item>)

    }

    @Test
    fun test_fetchRecomendedLocations_should_callError() {
        val mockCall: Call<ExploredPlaces> = mock()
        val mockThrowable: Throwable = mock()

        Mockito.`when`(api.getExplorePlaces(hashMap)).thenReturn(mockCall)
        Mockito.`when`(spHelper.getDataFromSharedPref(String::class.java, AppConstants.LATITUDE)).thenReturn("12.0")
        Mockito.`when`(spHelper.getDataFromSharedPref(String::class.java, AppConstants.LONGITUDE)).thenReturn("40.0")

        doAnswer {
            val callBack: Callback<ExploredPlaces> = it.getArgument(0)

            callBack.onFailure(mockCall, mockThrowable)
        }.`when`(mockCall).enqueue(any())

        presenter.fetchRecomendedLocations()

        Mockito.verify(view).onError()

    }


    @Test
    fun test_toggleSortType_Ascending_Success() {
        presenter.items = TestDataFactory.getTestResponse().response.groups.get(0).items as MutableList<Item>
        presenter.toggleSortType(true)
        Mockito.verify(spHelper).putDatatoSharedPref(true, Boolean::class.java, AppConstants.SORT_ASCENDING)
        Mockito.verify(spHelper).putDatatoSharedPref(false, Boolean::class.java, AppConstants.SORT_DESCENDING)
        Mockito.verify(view).enableAscendingSort()
    }

    @Test
    fun test_toggleSortType_Descending_Success() {
        presenter.items = TestDataFactory.getTestResponse().response.groups.get(0).items as MutableList<Item>
        presenter.toggleSortType(false)
        Mockito.verify(spHelper).putDatatoSharedPref(true, Boolean::class.java, AppConstants.SORT_DESCENDING)
        Mockito.verify(spHelper).putDatatoSharedPref(false, Boolean::class.java, AppConstants.SORT_ASCENDING)
        Mockito.verify(view).enableDescendingSort()
    }


}