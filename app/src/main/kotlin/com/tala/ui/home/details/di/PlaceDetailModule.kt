package com.tala.ui.home.details.di

import com.tala.base.api.EndPoints
import com.tala.base.di.ActivityScope
import com.tala.base.helper.SpHelper
import com.tala.ui.home.details.presenter.PlaceDetailsPresenter
import dagger.Module
import dagger.Provides

/**
 * Created by kavi on 03/12/17.
 */

@Module
class PlaceDetailModule {

    @Provides
    @ActivityScope
    internal fun providesPlaceDetailsPresenter(api: EndPoints, spHelper: SpHelper): PlaceDetailsPresenter {
        return PlaceDetailsPresenter(api, spHelper)
    }
}

