package com.tala.ui.home.details.presenter

import com.tala.base.di.mvp.BaseView
import com.tala.ui.home.details.model.Response

/**
 * Created by kavi on 03/12/17.
 */
interface PlaceDetailView : BaseView{

    fun populatePlaceDetails(response:Response)

}