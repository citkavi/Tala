package com.tala.ui.home.details.adapter

import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.facebook.drawee.view.SimpleDraweeView
import com.tala.ui.home.details.model.PhotosItem
import tala.com.tala_kavi.R


/**
 * Created by kavi on 04/12/17.
 */


class PhotoGridAdapter(private val mContext: Context, private val layoutResourceId: Int, mGridData: MutableList<PhotosItem>) : ArrayAdapter<PhotosItem>(mContext, layoutResourceId, mGridData) {


    private var mGridData: MutableList<PhotosItem> = mutableListOf()

    init {
        this.mGridData = mGridData
    }


    /**
     * Updates grid data and refresh grid items.
     * @param mGridData
     */
    fun setGridData(mGridData: MutableList<PhotosItem>) {
        this.mGridData = mGridData
        notifyDataSetChanged()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var row: View? = convertView
        val holder: ViewHolder

        if (row == null) {
            val inflater = (mContext as Activity).layoutInflater
            row = inflater.inflate(layoutResourceId, parent, false)
            holder = ViewHolder()
            holder.imgGridPhoto = row.findViewById<SimpleDraweeView>(R.id.imgGridPhoto)
            row!!.setTag(holder)
        } else {
            holder = row!!.getTag() as ViewHolder
        }

        val item = mGridData[position]
        Log.d("Photos",item.prefix + "90x90" +item.suffix)
        holder.imgGridPhoto?.setImageURI(item.prefix + "90x90" +item.suffix)

        return row
    }

    internal class ViewHolder {
        var imgGridPhoto: SimpleDraweeView? = null
    }
}