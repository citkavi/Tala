package com.tala.ui.home.details.di

import com.tala.base.di.ActivityScope
import com.tala.base.di.component.AppComponent
import com.tala.ui.home.details.PlaceDetailFragment
import com.tala.ui.home.di.PlacesListModule
import dagger.Component

/**
 * Created by kavi on 03/12/17.
 */
@ActivityScope
@Component(dependencies = arrayOf(AppComponent::class), modules = arrayOf(PlacesListModule::class))
interface PlaceDetailComponent {

    fun inject(placeDetailFragment: PlaceDetailFragment)
}
