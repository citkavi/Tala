package com.tala.ui.home.details.model

import com.google.gson.annotations.SerializedName


/**
 * Created by kavi on 03/12/17.
 */


data class VenueDetails(
        @SerializedName("meta") var meta: Meta = Meta(),
        @SerializedName("response") var response: Response
)

data class Meta(
        @SerializedName("code") var code: Int = 0, //200
        @SerializedName("requestId") var requestId: String = "" //5a245bd86a60716006410c8f
)

data class Response(
        @SerializedName("venue") var venue: Venue
)

data class Venue(
        @SerializedName("id") var id: String = "", //4f1d5288e4b044fd3768656b
        @SerializedName("name") var name: String = "", //Sree Annapoorna Gowrishankar
        @SerializedName("contact") var contact: Contact = Contact(),
        @SerializedName("location") var location: Location = Location(),
        @SerializedName("canonicalUrl") var canonicalUrl: String = "", //https://foursquare.com/v/sree-annapoorna-gowrishankar/4f1d5288e4b044fd3768656b
        @SerializedName("categories") var categories: List<Category> = listOf(),
        @SerializedName("verified") var verified: Boolean = false, //false
        @SerializedName("stats") var stats: Stats = Stats(),
        @SerializedName("url") var url: String = "", //http://sreeannapoorna.com
        @SerializedName("price") var price: Price = Price(),
        @SerializedName("likes") var likes: Likes = Likes(),
        @SerializedName("dislike") var dislike: Boolean = false, //false
        @SerializedName("ok") var ok: Boolean = false, //false
        @SerializedName("rating") var rating: Double = 0.0, //8.1
        @SerializedName("ratingColor") var ratingColor: String = "", //73CF42
        @SerializedName("ratingSignals") var ratingSignals: Int = 0, //44
        @SerializedName("allowMenuUrlEdit") var allowMenuUrlEdit: Boolean = false, //true
        @SerializedName("beenHere") var beenHere: BeenHere = BeenHere(),
        @SerializedName("specials") var specials: Specials = Specials(),
        @SerializedName("photos") var photos: Photos = Photos(),
        @SerializedName("reasons") var reasons: Reasons = Reasons(),
        @SerializedName("hereNow") var hereNow: HereNow = HereNow(),
        @SerializedName("createdAt") var createdAt: Int = 0, //1327321736
        @SerializedName("tips") var tips: Tips = Tips(),
        @SerializedName("shortUrl") var shortUrl: String = "", //http://4sq.com/xJ1IUM
        @SerializedName("timeZone") var timeZone: String = "", //Asia/Kolkata
        @SerializedName("listed") var listed: Listed,
        @SerializedName("phrases") var phrases: List<Phrase> = listOf(),
        @SerializedName("popular") var popular: Popular = Popular(),
        @SerializedName("pageUpdates") var pageUpdates: PageUpdates = PageUpdates(),
        @SerializedName("inbox") var inbox: Inbox = Inbox(),
        @SerializedName("venueChains") var venueChains: List<Any> = listOf(),
        @SerializedName("attributes") var attributes: Attributes = Attributes(),
        @SerializedName("bestPhoto") var bestPhoto: BestPhoto = BestPhoto(),
        @SerializedName("colors") var colors: Colors = Colors()
)

data class Attributes(
        @SerializedName("groups") var groups: List<AttributesGroup> = listOf()
)

data class AttributesGroup(
        @SerializedName("type") var type: String = "", //price
        @SerializedName("name") var name: String = "", //Price
        @SerializedName("summary") var summary: String = "", //₹₹
        @SerializedName("count") var count: Int = 0, //1
        @SerializedName("items") var items: List<AttributesItem> = listOf()
)

data class AttributesItem(
        @SerializedName("displayName") var displayName: String = "", //Price
        @SerializedName("displayValue") var displayValue: String = "", //₹₹
        @SerializedName("priceTier") var priceTier: Int = 0 //2
)

data class Popular(
        @SerializedName("isOpen") var isOpen: Boolean = false, //false
        @SerializedName("isLocalHoliday") var isLocalHoliday: Boolean = false, //false
        @SerializedName("timeframes") var timeframes: List<Timeframe> = listOf()
)

data class Timeframe(
        @SerializedName("days") var days: String = "", //Today
        @SerializedName("includesToday") var includesToday: Boolean = false, //true
        @SerializedName("open") var open: List<Open> = listOf(),
        @SerializedName("segments") var segments: List<Any> = listOf()
)

data class Open(
        @SerializedName("renderedTime") var renderedTime: String = "" //9:00 AM–11:00 AM
)

data class Location(
        @SerializedName("address") var address: String = "", //Saibaba colony
        @SerializedName("lat") var lat: Double = 0.0, //11.025535002082119
        @SerializedName("lng") var lng: Double = 0.0, //76.9424694611272
        @SerializedName("labeledLatLngs") var labeledLatLngs: List<LabeledLatLng> = listOf(),
        @SerializedName("cc") var cc: String = "", //IN
        @SerializedName("city") var city: String = "", //Coimbatore
        @SerializedName("state") var state: String = "", //Tamil Nadu
        @SerializedName("country") var country: String = "", //India
        @SerializedName("formattedAddress") var formattedAddress: List<String> = listOf()
)

data class LabeledLatLng(
        @SerializedName("label") var label: String = "", //display
        @SerializedName("lat") var lat: Double = 0.0, //11.025535002082119
        @SerializedName("lng") var lng: Double = 0.0 //76.9424694611272
)

data class Price(
        @SerializedName("tier") var tier: Int = 0, //2
        @SerializedName("message") var message: String = "", //Moderate
        @SerializedName("currency") var currency: String = "" //₹
)

data class BestPhoto(
        @SerializedName("id") var id: String = "", //53c63c8b498e2bdfd4760d64
        @SerializedName("createdAt") var createdAt: Int = 0, //1405500555
        @SerializedName("source") var source: Source = Source(),
        @SerializedName("prefix") var prefix: String = "", //https://igx.4sqi.net/img/general/
        @SerializedName("suffix") var suffix: String = "", ///13336739_M8t0coqQDPHj11hBS3ZjzykYCMLnEyJsaPQBPStFPT8.jpg
        @SerializedName("width") var width: Int = 0, //960
        @SerializedName("height") var height: Int = 0, //720
        @SerializedName("visibility") var visibility: String = "" //public
)

data class Source(
        @SerializedName("name") var name: String = "", //Swarm for Android
        @SerializedName("url") var url: String = "" //https://www.swarmapp.com
)

data class Inbox(
        @SerializedName("count") var count: Int = 0, //0
        @SerializedName("items") var items: List<Any> = listOf()
)

data class Likes(
        @SerializedName("count") var count: Int = 0, //28
        @SerializedName("groups") var groups: List<Group> = listOf(),
        @SerializedName("summary") var summary: String = "" //28 Likes
)

data class Group(
        @SerializedName("type") var type: String = "", //others
        @SerializedName("count") var count: Int = 0, //28
        @SerializedName("items") var items: List<Any> = listOf()
)

data class HereNow(
        @SerializedName("count") var count: Int = 0, //0
        @SerializedName("summary") var summary: String = "", //Nobody here
        @SerializedName("groups") var groups: List<Any> = listOf()
)

data class Photos(
        @SerializedName("count") var count: Int = 0, //6
        @SerializedName("groups") var groups: List<PhotosGroup> = listOf()
)

data class PhotosGroup(
        @SerializedName("type") var type: String = "", //venue
        @SerializedName("name") var name: String = "", //Venue photos
        @SerializedName("count") var count: Int = 0, //6
        @SerializedName("items") var items: List<PhotosItem> = listOf()
)

data class PhotosItem(
        @SerializedName("id") var id: String = "", //53c63c8b498e2bdfd4760d64
        @SerializedName("createdAt") var createdAt: Int = 0, //1405500555
        @SerializedName("source") var source: PhotosSource = PhotosSource(),
        @SerializedName("prefix") var prefix: String = "", //https://igx.4sqi.net/img/general/
        @SerializedName("suffix") var suffix: String = "", ///13336739_M8t0coqQDPHj11hBS3ZjzykYCMLnEyJsaPQBPStFPT8.jpg
        @SerializedName("width") var width: Int = 0, //960
        @SerializedName("height") var height: Int = 0, //720
        @SerializedName("user") var user: User = User(),
        @SerializedName("visibility") var visibility: String = "" //public
)

data class PhotosSource(
        @SerializedName("name") var name: String = "", //Swarm for Android
        @SerializedName("url") var url: String = "" //https://www.swarmapp.com
)

data class User(
        @SerializedName("id") var id: String = "", //13336739
        @SerializedName("firstName") var firstName: String = "", //Kalesh
        @SerializedName("lastName") var lastName: String = "", //Sivanandan
        @SerializedName("gender") var gender: String = "", //male
        @SerializedName("photo") var photo: Photo = Photo()
)

data class Photo(
        @SerializedName("prefix") var prefix: String = "", //https://igx.4sqi.net/img/user/
        @SerializedName("suffix") var suffix: String = "", ///KXB54BRMZTH0IJYS.jpg
        @SerializedName("default") var default: Boolean = false //true
)

data class Specials(
        @SerializedName("count") var count: Int = 0, //0
        @SerializedName("items") var items: List<Any> = listOf()
)

data class Category(
        @SerializedName("id") var id: String = "", //4bf58dd8d48988d10f941735
        @SerializedName("name") var name: String = "", //Indian Restaurant
        @SerializedName("pluralName") var pluralName: String = "", //Indian Restaurants
        @SerializedName("shortName") var shortName: String = "", //Indian
        @SerializedName("icon") var icon: Icon = Icon(),
        @SerializedName("primary") var primary: Boolean = false //true
)

data class Icon(
        @SerializedName("prefix") var prefix: String = "", //https://ss3.4sqi.net/img/categories_v2/food/indian_
        @SerializedName("suffix") var suffix: String = "" //.png
)

data class Contact(
        @SerializedName("phone") var phone: String = "", //4247448321
        @SerializedName("formattedPhone") var formattedPhone: String = "", //(424) 744-8321
        @SerializedName("twitter") var twitter: String = "", //sweetgreen
        @SerializedName("facebook") var facebook: String = "", //1684941461742004
        @SerializedName("facebookUsername") var facebookUsername: String = "", //sweetgreensantamonica
        @SerializedName("facebookName") var facebookName: String = "" //sweetgreen
)

data class Stats(
        @SerializedName("checkinsCount") var checkinsCount: Int = 0, //470
        @SerializedName("usersCount") var usersCount: Int = 0, //289
        @SerializedName("tipCount") var tipCount: Int = 0, //17
        @SerializedName("visitsCount") var visitsCount: Int = 0 //999
)

data class Tips(
        @SerializedName("count") var count: Int = 0, //17
        @SerializedName("groups") var groups: List<TipsGroup> = listOf()
)

data class TipsGroup(
        @SerializedName("type") var type: String = "", //others
        @SerializedName("name") var name: String = "", //All tips
        @SerializedName("count") var count: Int = 0, //17
        @SerializedName("items") var items: List<TipsItem> = listOf()
)

data class TipsItem(
        @SerializedName("id") var id: String = "", //503386c1e4b03bf9eeb8cf7f
        @SerializedName("createdAt") var createdAt: Int = 0, //1345554113
        @SerializedName("text") var text: String = "", //The best sambar ever.
        @SerializedName("type") var type: String = "", //user
        @SerializedName("canonicalUrl") var canonicalUrl: String = "", //https://foursquare.com/item/503386c1e4b03bf9eeb8cf7f
        @SerializedName("likes") var likes: TipsLikes = TipsLikes(),
        @SerializedName("logView") var logView: Boolean = false, //true
        @SerializedName("agreeCount") var agreeCount: Int = 0, //8
        @SerializedName("disagreeCount") var disagreeCount: Int = 0, //0
        @SerializedName("todo") var todo: Todo = Todo(),
        @SerializedName("user") var user: User = User(),
        @SerializedName("authorInteractionType") var authorInteractionType: String = "" //liked
)


data class TipsLikes(
        @SerializedName("count") var count: Int = 0, //8
        @SerializedName("groups") var groups: List<TipsLikeGroup> = listOf(),
        @SerializedName("summary") var summary: String = "" //8 likes
)

data class TipsLikeGroup(
        @SerializedName("type") var type: String = "", //others
        @SerializedName("count") var count: Int = 0, //8
        @SerializedName("items") var items: List<TipsLikeItem> = listOf()
)

data class TipsLikeItem(
        @SerializedName("id") var id: String = "", //10951091
        @SerializedName("firstName") var firstName: String = "", //Selva
        @SerializedName("lastName") var lastName: String = "", //Rajan
        @SerializedName("gender") var gender: String = "", //male
        @SerializedName("photo") var photo: Photo = Photo()
)


data class Todo(
        @SerializedName("count") var count: Int = 0 //2
)

data class Listed(
        @SerializedName("count") var count: Int = 0, //3
        @SerializedName("groups") var groups: List<ListedGroup> = listOf()
)

data class ListedGroup(
        @SerializedName("type") var type: String = "", //others
        @SerializedName("name") var name: String = "", //Lists from other people
        @SerializedName("count") var count: Int = 0, //3
        @SerializedName("items") var items: List<ListedItem> = listOf()
)

data class ListedItem(
        @SerializedName("id") var id: String = "", //51cd1c5c498edadea688592a
        @SerializedName("name") var name: String = "", //Favourite Hotels
        @SerializedName("description") var description: String = "",
        @SerializedName("type") var type: String = "", //others
        @SerializedName("user") var user: User = User(),
        @SerializedName("editable") var editable: Boolean = false, //false
        @SerializedName("public") var public: Boolean = false, //true
        @SerializedName("collaborative") var collaborative: Boolean = false, //false
        @SerializedName("url") var url: String = "", ///user/59622904/list/favourite-hotels
        @SerializedName("canonicalUrl") var canonicalUrl: String = "", //https://foursquare.com/user/59622904/list/favourite-hotels
        @SerializedName("createdAt") var createdAt: Int = 0, //1372396636
        @SerializedName("updatedAt") var updatedAt: Int = 0, //1372396782
        @SerializedName("photo") var photo: ListedPhoto = ListedPhoto(),
        @SerializedName("followers") var followers: Followers = Followers(),
        @SerializedName("listItems") var listItems: ListItems
)

data class ListItems(
        @SerializedName("count") var count: Int = 0, //7
        @SerializedName("items") var items: List<Item> = listOf()
)

data class Item(
        @SerializedName("id") var id: String = "", //v4f1d5288e4b044fd3768656b
        @SerializedName("createdAt") var createdAt: Int = 0 //1372396728
)

data class Followers(
        @SerializedName("count") var count: Int = 0 //1
)

data class ListedPhoto(
        @SerializedName("id") var id: String = "", //508a5088e4b06f6d18275273
        @SerializedName("createdAt") var createdAt: Int = 0, //1351241864
        @SerializedName("prefix") var prefix: String = "", //https://igx.4sqi.net/img/general/
        @SerializedName("suffix") var suffix: String = "", ///25550108_fh7GyIvJt_ZrLiYmczE47b_HtNQWqmyYX607MPfzQwI.jpg
        @SerializedName("width") var width: Int = 0, //540
        @SerializedName("height") var height: Int = 0, //540
        @SerializedName("user") var user: User = User(),
        @SerializedName("visibility") var visibility: String = "" //public
)


data class PageUpdates(
        @SerializedName("count") var count: Int = 0, //0
        @SerializedName("items") var items: List<Any> = listOf()
)

data class Colors(
        @SerializedName("highlightColor") var highlightColor: HighlightColor = HighlightColor(),
        @SerializedName("highlightTextColor") var highlightTextColor: HighlightTextColor = HighlightTextColor(),
        @SerializedName("algoVersion") var algoVersion: Int = 0 //3
)

data class HighlightTextColor(
        @SerializedName("photoId") var photoId: String = "", //53c63c8b498e2bdfd4760d64
        @SerializedName("value") var value: Int = 0 //-1
)

data class HighlightColor(
        @SerializedName("photoId") var photoId: String = "", //53c63c8b498e2bdfd4760d64
        @SerializedName("value") var value: Int = 0 //-13092816
)

data class BeenHere(
        @SerializedName("count") var count: Int = 0, //0
        @SerializedName("unconfirmedCount") var unconfirmedCount: Int = 0, //0
        @SerializedName("marked") var marked: Boolean = false, //false
        @SerializedName("lastCheckinExpiredAt") var lastCheckinExpiredAt: Int = 0 //0
)

data class Reasons(
        @SerializedName("count") var count: Int = 0, //0
        @SerializedName("items") var items: List<Any> = listOf()
)

data class Phrase(
        @SerializedName("phrase") var phrase: String = "", //best sambar
        @SerializedName("sample") var sample: Sample = Sample(),
        @SerializedName("count") var count: Int = 0 //2
)

data class Sample(
        @SerializedName("entities") var entities: List<Entity> = listOf(),
        @SerializedName("text") var text: String = "" //The best sambar ever.
)

data class Entity(
        @SerializedName("indices") var indices: List<Int> = listOf(),
        @SerializedName("type") var type: String = "" //keyPhrase
)