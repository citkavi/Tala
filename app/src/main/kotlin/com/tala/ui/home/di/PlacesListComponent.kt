package com.tala.ui.home.di

import com.tala.base.di.ActivityScope
import com.tala.base.di.component.AppComponent
import com.tala.ui.home.PlaceListActivity
import dagger.Component

/**
 * Created by kavi on 03/12/17.
 */

@ActivityScope
@Component(dependencies = arrayOf(AppComponent::class), modules = arrayOf(PlacesListModule::class))
interface PlacesListComponent {

    fun inject(placeListActivity: PlaceListActivity)
}
