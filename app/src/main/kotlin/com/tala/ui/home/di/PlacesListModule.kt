package com.tala.ui.home.di

import com.tala.base.api.EndPoints
import com.tala.base.di.ActivityScope
import com.tala.base.helper.SpHelper
import com.tala.ui.home.presenter.PlacesListPresenter
import dagger.Module
import dagger.Provides

/**
 * Created by kavi on 03/12/17.
 */


@Module
class PlacesListModule {

    @Provides
    @ActivityScope
    internal fun providesPlacesListPresenter(api: EndPoints, spHelper: SpHelper): PlacesListPresenter {
        return PlacesListPresenter(api, spHelper)
    }
}
