package com.tala.ui.home.details

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.AppCompatTextView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridView
import android.widget.Toast
import com.facebook.drawee.view.SimpleDraweeView
import com.tala.base.BaseFragment
import com.tala.ui.home.details.adapter.PhotoGridAdapter
import com.tala.ui.home.details.di.DaggerPlaceDetailComponent
import com.tala.ui.home.details.model.PhotosItem
import com.tala.ui.home.details.model.Response
import com.tala.ui.home.details.model.TipsItem
import com.tala.ui.home.details.model.Venue
import com.tala.ui.home.details.presenter.PlaceDetailView
import com.tala.ui.home.details.presenter.PlaceDetailsPresenter
import com.tala.ui.home.di.PlacesListModule
import kotlinx.android.synthetic.main.activity_place_detail.*
import kotlinx.android.synthetic.main.activity_place_detail.view.*
import kotlinx.android.synthetic.main.place_detail.*
import tala.com.tala_kavi.R
import javax.inject.Inject


/**
 * A fragment representing a single Place detail screen.
 * This fragment is either contained in a [PlaceListActivity]
 * in two-pane mode (on tablets) or a [PlaceDetailActivity]
 * on handsets.
 */
class PlaceDetailFragment : BaseFragment(), PlaceDetailView {


    val MY_PERMISSIONS_REQUEST_CALL = 1

    var mPhotoAdapter: PhotoGridAdapter? = null

    var mPhotoItem: MutableList<PhotosItem> = mutableListOf()


    @Inject
    lateinit var presenter: PlaceDetailsPresenter

    private var mVenueId: String? = ""

    private var mVenue: Venue? = null


    override fun onActivityInject() {
        DaggerPlaceDetailComponent.builder().appComponent(getAppcomponent())
                .placesListModule(PlacesListModule())
                .build()
                .inject(this)
        presenter.attachView(this)

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (arguments.containsKey(ARG_ITEM_ID)) {
            mVenueId = arguments.getSerializable(ARG_ITEM_ID) as String?


        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.place_detail, container, false)


        presenter.fetchLocationDetails(this!!.mVenueId!!)

        return rootView
    }

    companion object {
        /**
         * The fragment argument representing the item ID that this fragment
         * represents.
         */
        const val ARG_ITEM_ID = "item_id"
    }

    override fun populatePlaceDetails(response: Response) {

        mVenue = response.venue

        response?.let {
            activity.toolbar_layout?.title = mVenue!!.name
            if (response.venue.bestPhoto != null)
                activity.toolbar_layout.imgHeaderImg.setImageURI(mVenue!!.bestPhoto.prefix + "original" + mVenue!!.bestPhoto.suffix)

            activity.fab.setOnClickListener { view ->

                if (mVenue!!.location.lat != null) {
                    val intent = Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("http://maps.google.com/maps?saddr=" + presenter.getLatLng() + "&daddr=" + mVenue!!.location.lat + "," + mVenue!!.location.lng))
                    startActivity(intent)
                }
            }

            populateData()
            setUpGrid()

        }
    }

    private fun populateData() {

        handleCategories()

        handleAddress()

        handlePhoneView()

        handleWebLinkView()

        txtCheckIn.text = mVenue!!.stats.checkinsCount.toString()
        txtUsers.text = mVenue!!.stats.usersCount.toString()
        txtTips.text = mVenue!!.stats.tipCount.toString()
        txtVisits.text = mVenue!!.stats.visitsCount.toString()


        handleTips()

        handlePhotos()


    }

    private fun handlePhotos() {
        if (mVenue!!.photos != null) {
            mPhotoItem.clear()
            for (group in mVenue!!.photos.groups)
                for (item in group.items)
                    mPhotoItem.add(item)
            mPhotoAdapter?.setGridData(mPhotoItem)
            setDynamicHeight(photosGrid)
        }
    }

    private fun handleTips() {
        for (group in mVenue!!.tips.groups)
            for (item in group.items)
                addDescriptionLayout(item)
    }

    private fun handleAddress() {
        var displayText: String = ""
        for (formattedAddress in mVenue!!.location.formattedAddress) {
            if (displayText.length > 0)
                displayText = displayText + "," + formattedAddress
            else
                displayText = formattedAddress
        }
        txtAddress.text = displayText
    }

    private fun handleCategories() {
        var displayText: String = ""
        for (category in mVenue?.categories!!) {
            if (displayText.length > 0)
                displayText = displayText + "," + category.name
            else
                displayText = category.name
        }

        txtCategory.text = displayText
    }

    private fun handlePhoneView() {
        if (mVenue!!.contact.phone.length > 0) {
            txtPhone.text = mVenue!!.contact.phone

            txtPhone.setOnClickListener { view ->
                checkCallPermission()
            }
        } else
            txtPhone.visibility = View.GONE
    }

    private fun handleWebLinkView() {
        if (mVenue!!.url!=null && mVenue!!.url.length > 0) {
            txtWeb.text = mVenue!!.url
            var url: String = ""
            if (!mVenue!!.url.startsWith("http://") && !mVenue!!.url.startsWith("https://"))
                url = "http://" + mVenue!!.url;
            else
                url = mVenue!!.url

            txtWeb.setOnClickListener { view ->
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))

            }

        } else
            txtWeb.visibility = View.GONE
    }


    private fun setUpGrid() {
        mPhotoAdapter = PhotoGridAdapter(activity, R.layout.view_photo_grid, mPhotoItem)
        photosGrid.setAdapter(mPhotoAdapter)

    }

    private fun setDynamicHeight(gridView: GridView) {
        val gridViewAdapter = gridView.adapter ?: // pre-condition
                return

        var totalHeight = 0
        val items = gridViewAdapter.count
        var rows = 0

        val listItem = gridViewAdapter.getView(0, null, gridView)
        listItem.measure(0, 0)
        totalHeight = listItem.measuredHeight

        var x = 1f
        if (items > 5) {
            x = (items / 5).toFloat()
            rows = (x + 1).toInt()
            totalHeight *= rows
        }

        val params = gridView.layoutParams
        params.height = totalHeight
        gridView.layoutParams = params
    }

    fun addDescriptionLayout(item: TipsItem) {
        val inflater = LayoutInflater.from(context)
        val child = inflater.inflate(R.layout.view_user_reviews, null)
        child.findViewById<SimpleDraweeView>(R.id.imgUser).setImageURI(item.user.photo.prefix + "80x80" + item.user.photo.suffix)
        child.findViewById<AppCompatTextView>(R.id.txtUserName).text = item.user.firstName + " " + item.user.lastName
        child.findViewById<AppCompatTextView>(R.id.txtUserComments).text = item.text
        llComments.addView(child)

    }


    private fun callToPhone() {
        val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + mVenue!!.contact.phone))
        startActivity(intent)
    }

    private fun checkCallPermission() {

        val permissionArray = arrayOf(Manifest.permission.CALL_PHONE)


        if (ContextCompat.checkSelfPermission(activity,
                Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                    Manifest.permission.READ_CONTACTS)) {

                showRationaleDialog(R.string.permission_phone_rationale)

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(activity,
                        permissionArray,
                        MY_PERMISSIONS_REQUEST_CALL);

            }
        } else
            callToPhone()
    }


    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_CALL -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    callToPhone()

                }
                return
            }
        }
    }

    private fun showRationaleDialog(@StringRes messageResId: Int) {
        AlertDialog.Builder(activity)
                .setPositiveButton(R.string.button_allow) { _, _ -> callToPhone() }
                .setNegativeButton(R.string.button_deny) { _, _ -> Toast.makeText(activity, "", Toast.LENGTH_LONG) }
                .setCancelable(false)
                .setMessage(messageResId)
                .show()
    }
}
