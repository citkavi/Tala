package com.tala.ui.home.model

import com.google.gson.annotations.SerializedName


/**
 * Created by kavi on 03/12/17.
 */



    data class ExploredPlaces(
        @SerializedName("meta") var meta: Meta = Meta(),
        @SerializedName("response") var response: Response = Response()
    )

    data class Response(
            @SerializedName("suggestedRadius") var suggestedRadius: Int = 0, //6767
            @SerializedName("headerLocation") var headerLocation: String = "", //Coimbatore
            @SerializedName("headerFullLocation") var headerFullLocation: String = "", //Coimbatore
            @SerializedName("headerLocationGranularity") var headerLocationGranularity: String = "", //city
            @SerializedName("totalResults") var totalResults: Int = 0, //68
            @SerializedName("suggestedBounds") var suggestedBounds: SuggestedBounds = SuggestedBounds(),
            @SerializedName("groups") var groups: List<Group> = listOf()
    )

    data class SuggestedBounds(
            @SerializedName("ne") var ne: Ne = Ne(),
            @SerializedName("sw") var sw: Sw = Sw()
    )

    data class Sw(
            @SerializedName("lat") var lat: Double = 0.0, //11.002702071416012
            @SerializedName("lng") var lng: Double = 0.0 //76.93693907371271
    )

    data class Ne(
            @SerializedName("lat") var lat: Double = 0.0, //11.048539405871574
            @SerializedName("lng") var lng: Double = 0.0 //76.99040188855686
    )

    data class Group(
            @SerializedName("type") var type: String = "", //Recommended Places
            @SerializedName("name") var name: String = "", //recommended
            @SerializedName("items") var items: List<Item> = listOf()
    )

    data class Item(
            @SerializedName("reasons") var reasons: Reasons = Reasons(),
            @SerializedName("venue") var venue: Venue = Venue(),
            @SerializedName("tips") var tips: List<Tip> = listOf(),
            @SerializedName("referralId") var referralId: String = "" //e-0-4f1d5288e4b044fd3768656b-0
    )

    data class Reasons(
            @SerializedName("count") var count: Int = 0, //0
            @SerializedName("items") var items: List<Item_> = listOf()
    )

    data class Item_(
            @SerializedName("summary") var summary: String = "", //This spot is popular
            @SerializedName("type") var type: String = "", //general
            @SerializedName("reasonName") var reasonName: String = "" //globalInteractionReason
    )

    data class Tip(
            @SerializedName("id") var id: String = "", //503386c1e4b03bf9eeb8cf7f
            @SerializedName("createdAt") var createdAt: Int = 0, //1345554113
            @SerializedName("text") var text: String = "", //The best sambar ever.
            @SerializedName("type") var type: String = "", //user
            @SerializedName("canonicalUrl") var canonicalUrl: String = "", //https://foursquare.com/item/503386c1e4b03bf9eeb8cf7f
            @SerializedName("likes") var likes: Likes = Likes(),
            @SerializedName("logView") var logView: Boolean = false, //true
            @SerializedName("agreeCount") var agreeCount: Int = 0, //8
            @SerializedName("disagreeCount") var disagreeCount: Int = 0, //0
            @SerializedName("todo") var todo: Todo = Todo(),
            @SerializedName("user") var user: User = User()
    )


    data class Likes(
            @SerializedName("count") var count: Int = 0, //8
            @SerializedName("groups") var groups: List<Any> = listOf(),
            @SerializedName("summary") var summary: String = "" //8 likes
    )

    data class Todo(
            @SerializedName("count") var count: Int = 0 //2
    )

    data class Venue(
            @SerializedName("id") var id: String = "", //4f1d5288e4b044fd3768656b
            @SerializedName("name") var name: String = "", //Sree Annapoorna Gowrishankar
            @SerializedName("contact") var contact: Contacts = Contacts(),
            @SerializedName("location") var location: Location = Location(),
            @SerializedName("categories") var categories: List<Category> = listOf(),
            @SerializedName("verified") var verified: Boolean = false, //false
            @SerializedName("stats") var stats: Stats = Stats(),
            @SerializedName("url") var url: String = "", //http://sreeannapoorna.com
            @SerializedName("price") var price: Price = Price(),
            @SerializedName("rating") var rating: Double = 0.0, //8.1
            @SerializedName("ratingColor") var ratingColor: String = "", //73CF42
            @SerializedName("ratingSignals") var ratingSignals: Int = 0, //44
            @SerializedName("allowMenuUrlEdit") var allowMenuUrlEdit: Boolean = false, //true
            @SerializedName("beenHere") var beenHere: BeenHere = BeenHere(),
            @SerializedName("hours") var hours: Hours = Hours(),
            @SerializedName("photos") var photos: Photos = Photos(),
            @SerializedName("hereNow") var hereNow: HereNow = HereNow(),
            @SerializedName("featuredPhotos") var featuredPhotos: FeaturedPhotos = FeaturedPhotos()
    )

    data class HereNow(
            @SerializedName("count") var count: Int = 0, //0
            @SerializedName("summary") var summary: String = "", //Nobody here
            @SerializedName("groups") var groups: List<Any> = listOf()
    )

    data class BeenHere(
            @SerializedName("count") var count: Int = 0, //0
            @SerializedName("marked") var marked: Boolean = false, //false
            @SerializedName("lastCheckinExpiredAt") var lastCheckinExpiredAt: Int = 0 //0
    )

    data class Category(
            @SerializedName("id") var id: String = "", //4bf58dd8d48988d10f941735
            @SerializedName("name") var name: String = "", //Indian Restaurant
            @SerializedName("pluralName") var pluralName: String = "", //Indian Restaurants
            @SerializedName("shortName") var shortName: String = "", //Indian
            @SerializedName("icon") var icon: Icon = Icon(),
            @SerializedName("primary") var primary: Boolean = false //true
    )

    data class Icon(
            @SerializedName("prefix") var prefix: String = "", //https://ss3.4sqi.net/img/categories_v2/food/indian_
            @SerializedName("suffix") var suffix: String = "" //.png
    )

    data class Location(
            @SerializedName("address") var address: String = "", //Saibaba colony
            @SerializedName("lat") var lat: Double = 0.0, //11.025535002082119
            @SerializedName("lng") var lng: Double = 0.0, //76.9424694611272
            @SerializedName("labeledLatLngs") var labeledLatLngs: List<LabeledLatLng> = listOf(),
            @SerializedName("distance") var distance: Int = 0, //3885
            @SerializedName("cc") var cc: String = "", //IN
            @SerializedName("city") var city: String = "", //Coimbatore
            @SerializedName("state") var state: String = "", //Tamil Nadu
            @SerializedName("country") var country: String = "", //India
            @SerializedName("formattedAddress") var formattedAddress: List<String> = listOf()
    )

    data class LabeledLatLng(
            @SerializedName("label") var label: String = "", //display
            @SerializedName("lat") var lat: Double = 0.0, //11.025535002082119
            @SerializedName("lng") var lng: Double = 0.0 //76.9424694611272
    )

    data class Contacts(
            @SerializedName("phone") var phone: String = "", //4247448321
            @SerializedName("formattedPhone") var formattedPhone: String = "", //(424) 744-8321
            @SerializedName("twitter") var twitter: String = "", //sweetgreen
            @SerializedName("facebook") var facebook: String = "", //1684941461742004
            @SerializedName("facebookUsername") var facebookUsername: String = "", //sweetgreensantamonica
            @SerializedName("facebookName") var facebookName: String = "" //sweetgreen
    )


    data class Hours(
            @SerializedName("isOpen") var isOpen: Boolean = false, //false
            @SerializedName("isLocalHoliday") var isLocalHoliday: Boolean = false //false
    )

    data class Price(
            @SerializedName("tier") var tier: Int = 0, //2
            @SerializedName("message") var message: String = "", //Moderate
            @SerializedName("currency") var currency: String = "" //₹
    )

    data class FeaturedPhotos(
            @SerializedName("count") var count: Int = 0, //1
            @SerializedName("items") var items: List<PhotoItem> = listOf()
    )

    data class PhotoItem(
            @SerializedName("id") var id: String = "", //53c63c8b498e2bdfd4760d64
            @SerializedName("createdAt") var createdAt: Int = 0, //1405500555
            @SerializedName("prefix") var prefix: String = "", //https://igx.4sqi.net/img/general/
            @SerializedName("suffix") var suffix: String = "", ///13336739_M8t0coqQDPHj11hBS3ZjzykYCMLnEyJsaPQBPStFPT8.jpg
            @SerializedName("width") var width: Int = 0, //960
            @SerializedName("height") var height: Int = 0, //720
            @SerializedName("user") var user: User = User(),
            @SerializedName("visibility") var visibility: String = "" //public
    )

    data class User(
            @SerializedName("id") var id: String = "", //13336739
            @SerializedName("firstName") var firstName: String = "", //Kalesh
            @SerializedName("lastName") var lastName: String = "", //Sivanandan
            @SerializedName("gender") var gender: String = "", //male
            @SerializedName("photo") var photo: Photo = Photo()
    )

    data class Photo(
            @SerializedName("prefix") var prefix: String = "", //https://igx.4sqi.net/img/user/
            @SerializedName("suffix") var suffix: String = "" ///KXB54BRMZTH0IJYS.jpg
    )

    data class Stats(
            @SerializedName("checkinsCount") var checkinsCount: Int = 0, //470
            @SerializedName("usersCount") var usersCount: Int = 0, //160
            @SerializedName("tipCount") var tipCount: Int = 0 //17
    )

    data class Photos(
            @SerializedName("count") var count: Int = 0, //1
            @SerializedName("groups") var groups: List<Group_> = listOf()
    )

    data class Group_(
            @SerializedName("type") var type: String = "", //venue
            @SerializedName("name") var name: String = "", //Venue photos
            @SerializedName("count") var count: Int = 0, //1
            @SerializedName("items") var items: List<PhotoItem> = listOf()
    )


    data class Meta(
            @SerializedName("code") var code: Int = 0, //200
            @SerializedName("requestId") var requestId: String = "" //5a244da3db04f52b3e35d01a
    )
