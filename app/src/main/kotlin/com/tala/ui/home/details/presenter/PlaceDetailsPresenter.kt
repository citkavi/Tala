package com.tala.ui.home.details.presenter

import com.tala.base.AppConstants
import com.tala.base.api.EndPoints
import com.tala.base.di.mvp.BasePresenter
import com.tala.base.helper.SpHelper
import com.tala.ui.home.details.model.VenueDetails
import retrofit2.Call
import retrofit2.Response
import tala.com.tala_kavi.BuildConfig
import javax.inject.Inject

/**
 * Created by kavi on 03/12/17.
 */

class PlaceDetailsPresenter @Inject constructor(var api: EndPoints, var spHelper: SpHelper) : BasePresenter<PlaceDetailView>() {


    fun fetchLocationDetails(venueId: String) {
        val hashMap = HashMap<String, Any>()
        hashMap.put("client_id", BuildConfig.CLIENT_ID)
        hashMap.put("client_secret", BuildConfig.CLIENT_SECRET)
        hashMap.put("v", BuildConfig.API_VERSION)


        api.getPlacesDetails(venueId, hashMap).enqueue(object : retrofit2.Callback<VenueDetails> {
            override fun onResponse(call: Call<VenueDetails>, response: Response<VenueDetails>) {
                  view?.populatePlaceDetails(response.body().response)

            }

            override fun onFailure(call: Call<VenueDetails>, t: Throwable) {
                view?.onError()
            }
        })

    }

    fun getLatLng():String{
        return ""+spHelper.getDataFromSharedPref(String::class.java, AppConstants.LATITUDE)!!+","+spHelper.getDataFromSharedPref(String::class.java,AppConstants.LONGITUDE)!!
    }

}