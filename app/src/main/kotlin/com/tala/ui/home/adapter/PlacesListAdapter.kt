package com.tala.ui.home.adapter

import android.graphics.Color
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.CardView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.facebook.drawee.view.SimpleDraweeView
import com.paging.gridview.PagingBaseAdapter
import com.tala.ui.home.PlaceListActivity
import com.tala.ui.home.details.PlaceDetailActivity
import com.tala.ui.home.details.PlaceDetailFragment
import com.tala.ui.home.model.Item
import tala.com.tala_kavi.R

/**
 * Created by kavi on 03/12/17.
 */


class PlacesListAdapter(private val mParentActivity: PlaceListActivity,
                        private val mTwoPane: Boolean) : PagingBaseAdapter<Item>() {


    override fun getCount(): Int {
        return items.size
    }

    override fun getItem(position: Int): Item? {
        return items[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val textView: View
        val item = getItem(position)

        if (convertView != null) {
            textView = convertView
        } else {
            textView = LayoutInflater.from(parent.context).inflate(R.layout.place_list_content, null) as CardView
        }

        textView.findViewById<AppCompatTextView>(R.id.txtName).text = item?.venue?.name


        if (item?.venue?.featuredPhotos?.items?.size!! > 0) {
            textView.findViewById<SimpleDraweeView>(R.id.imgThumbNail).setImageURI(item?.venue?.featuredPhotos?.items?.get(0)?.prefix + "150x150" + item?.venue?.featuredPhotos?.items?.get(0)?.suffix)
        }
        if (item?.venue?.categories!!.isNotEmpty()) {
            for (category in item?.venue?.categories)
                if (category.primary) {
                    textView.findViewById<AppCompatTextView>(R.id.txtCategory).text = category.name
                    break
                }
        }

        textView.findViewById<AppCompatTextView>(R.id.txtDistance).text = String.format("%.2f", item.venue.location.distance * 0.001) + " Km away"
        textView.findViewById<AppCompatTextView>(R.id.txtRating).text = item.venue.rating.toString() + " Stars"
        if (item.venue.ratingColor.length > 0)
            textView.findViewById<AppCompatTextView>(R.id.txtRating).setTextColor(Color.parseColor("#" + item.venue.ratingColor));

        with(textView) {
            setOnClickListener(android.view.View.OnClickListener { v ->
                if (mTwoPane) {
                    val fragment = PlaceDetailFragment().apply {
                        arguments = android.os.Bundle()
                        arguments.putSerializable(PlaceDetailFragment.ARG_ITEM_ID, item.venue.id)
                    }
                    mParentActivity.supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.place_detail_container, fragment)
                            .commit()
                } else {
                    val intent = android.content.Intent(v.context, PlaceDetailActivity::class.java).apply {
                        putExtra(PlaceDetailFragment.ARG_ITEM_ID, item.venue.id)
                    }
                    v.context.startActivity(intent)
                }
            })
        }

        // textView.item = item
        return textView
    }
}