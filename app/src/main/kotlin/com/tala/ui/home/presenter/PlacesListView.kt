package com.tala.ui.home.presenter

import com.tala.base.di.mvp.BaseView
import com.tala.ui.home.model.Item

/**
 * Created by kavi on 03/12/17.
 */
interface PlacesListView: BaseView {


    fun populateRecomendedVenues(items:MutableList<Item>)

    fun enableSortByDistance()

    fun enableSortByRating()

    fun enableAscendingSort()

    fun enableDescendingSort()

}