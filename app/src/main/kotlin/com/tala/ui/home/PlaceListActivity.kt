package com.tala.ui.home

import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.v4.widget.SwipeRefreshLayout
import android.view.Menu
import android.view.MenuItem
import com.paging.gridview.FooterViewGridAdapter
import com.paging.gridview.PagingGridView
import com.tala.base.BaseActivity
import com.tala.ui.home.adapter.PlacesListAdapter
import com.tala.ui.home.details.PlaceDetailActivity
import com.tala.ui.home.di.DaggerPlacesListComponent
import com.tala.ui.home.di.PlacesListModule
import com.tala.ui.home.model.Item
import com.tala.ui.home.presenter.PlacesListPresenter
import com.tala.ui.home.presenter.PlacesListView
import kotlinx.android.synthetic.main.activity_place_list.*
import kotlinx.android.synthetic.main.place_list.*
import kotlinx.android.synthetic.main.view_sort_list.*
import tala.com.tala_kavi.R
import javax.inject.Inject


/**
 * An activity representing a list of Pings. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a [PlaceDetailActivity] representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
class PlaceListActivity : BaseActivity(), PlacesListView {


    override fun populateRecomendedVenues(items: MutableList<Item>) {
        clearData()
        pager++
        refreshLayout.isRefreshing = false
        paging_grid_view.onFinishLoading(true, items);
        behavior?.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }


    @Inject
    lateinit var presenter: PlacesListPresenter

    private lateinit var adapter: PlacesListAdapter;

    var behavior: BottomSheetBehavior<*>? = null

    override fun onActivityInject() {

        DaggerPlacesListComponent.builder().appComponent(getAppcomponent())
                .placesListModule(PlacesListModule())
                .build()
                .inject(this)

        presenter.attachView(this)

    }


    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private var mTwoPane: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_place_list)
        setUpSortListeners()
        behavior = BottomSheetBehavior.from(bottomSheet)


        setSupportActionBar(toolbar)
        toolbar.title = getString(R.string.list_header)


        if (place_detail_container != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true
        }

        adapter = PlacesListAdapter(this, mTwoPane)
        paging_grid_view.adapter = adapter

        presenter.fetchRecomendedLocations()


        paging_grid_view.setPagingableListener(PagingGridView.Pagingable {
            paging_grid_view.onFinishLoading(false, null)

        })



        refreshLayout.setOnRefreshListener(
                SwipeRefreshLayout.OnRefreshListener {
                    clearData()
                    pager = 0
                    presenter.fetchRecomendedLocations()
                }
        )
    }


    var pager = 0;

    fun clearData() {

        adapter = (paging_grid_view.adapter as FooterViewGridAdapter).wrappedAdapter as PlacesListAdapter
        adapter.removeAllItems()

        adapter = PlacesListAdapter(this, mTwoPane)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_list_screen, menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
        // action with ID action_refresh was selected
            R.id.action_sort -> {
                if (behavior?.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                    behavior?.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    behavior?.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
            }

        }

        return true
    }


    private fun setUpSortListeners() {

        txtSortByDistance.setOnClickListener { view ->
            run {
                presenter.toggleSortBy(true)
            }
        }

        txtSortByRating.setOnClickListener { view ->
            run {
                presenter.toggleSortBy(false)
            }
        }

        txtSortAscending.setOnClickListener { view ->
            run {
                presenter.toggleSortType(true)
            }
        }

        txtSortDescending.setOnClickListener { view ->
            run {
                presenter.toggleSortType(false)
            }
        }

    }


    override fun enableSortByDistance() {
        txtSortByDistance.setTextColor(resources.getColor(R.color.selected_text_color))
        txtSortByDistance.setCompoundDrawablesWithIntrinsicBounds(null, getDrawable(R.drawable.ic_distance_selected_24dp), null, null)

        txtSortByRating.setTextColor(resources.getColor(android.R.color.white))
        txtSortByRating.setCompoundDrawablesWithIntrinsicBounds(null, getDrawable(R.drawable.ic_star_24dp), null, null)

    }

    override fun enableSortByRating() {
        txtSortByDistance.setTextColor(resources.getColor(android.R.color.white))
        txtSortByDistance.setCompoundDrawablesWithIntrinsicBounds(null, getDrawable(R.drawable.ic_distance_24dp), null, null)

        txtSortByRating.setTextColor(resources.getColor(R.color.selected_text_color))
        txtSortByRating.setCompoundDrawablesWithIntrinsicBounds(null, getDrawable(R.drawable.ic_star_selected), null, null)

    }

    override fun enableAscendingSort() {

        txtSortAscending.setTextColor(resources.getColor(R.color.selected_text_color))
        txtSortAscending.setCompoundDrawablesWithIntrinsicBounds(null, getDrawable(R.drawable.ic_arrow_downward_selected), null, null)

        txtSortDescending.setTextColor(resources.getColor(android.R.color.white))
        txtSortDescending.setCompoundDrawablesWithIntrinsicBounds(null, getDrawable(R.drawable.ic_arrow_upward_24dp), null, null)

    }

    override fun enableDescendingSort() {
        txtSortDescending.setTextColor(resources.getColor(R.color.selected_text_color))
        txtSortDescending.setCompoundDrawablesWithIntrinsicBounds(null, getDrawable(R.drawable.ic_arrow_upward_selected), null, null)

        txtSortAscending.setTextColor(resources.getColor(android.R.color.white))
        txtSortAscending.setCompoundDrawablesWithIntrinsicBounds(null, getDrawable(R.drawable.ic_arrow_downward_24dp), null, null)

    }


}
