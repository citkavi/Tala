package com.tala.ui.home.presenter

import com.tala.base.AppConstants
import com.tala.base.api.EndPoints
import com.tala.base.di.mvp.BasePresenter
import com.tala.base.helper.SpHelper
import com.tala.ui.home.model.ExploredPlaces
import com.tala.ui.home.model.Item
import retrofit2.Call
import retrofit2.Response
import tala.com.tala_kavi.BuildConfig
import javax.inject.Inject

/**
 * Created by kavi on 03/12/17.
 */


class PlacesListPresenter @Inject constructor(var api: EndPoints, var spHelper: SpHelper) : BasePresenter<PlacesListView>() {


    private var isDistanceSort: Boolean = true

    private var isAscendingSort: Boolean = true

    var items: MutableList<Item>? = null

    fun fetchRecomendedLocations() {

        val hashMap = HashMap<String, Any>()
        hashMap.put("client_id", BuildConfig.CLIENT_ID)
        hashMap.put("client_secret", BuildConfig.CLIENT_SECRET)
        hashMap.put("v", BuildConfig.API_VERSION)
        hashMap.put("ll", "" + spHelper.getDataFromSharedPref(String::class.java, AppConstants.LATITUDE)!! + "," + spHelper.getDataFromSharedPref(String::class.java, AppConstants.LONGITUDE)!!)
        hashMap.put("venuePhotos", 1)


        api.getExplorePlaces(hashMap).enqueue(object : retrofit2.Callback<ExploredPlaces> {
            override fun onResponse(call: Call<ExploredPlaces>, response: Response<ExploredPlaces>) {

                items= mutableListOf()
                for (group in response.body().response.groups)
                    for (item in group.items)
                        items!!.add(item)

                view?.populateRecomendedVenues(items!!)
            }

            override fun onFailure(call: Call<ExploredPlaces>, t: Throwable) {
                view?.onError()
            }
        })

    }


    fun sortByAscendingRating(items: MutableList<Item>) {
        view?.populateRecomendedVenues(items.sortedBy { it.venue.rating } as MutableList<Item>)

    }

    fun sortByDescendingRating(items: MutableList<Item>) {
        view?.populateRecomendedVenues(items.sortedBy { it.venue.rating }.reversed() as MutableList<Item>)

    }

    fun sortByAscendingDistance(items: MutableList<Item>) {
        view?.populateRecomendedVenues(items.sortedBy { it.venue.location.distance } as MutableList<Item>)

    }

    fun sortByDescendingDistance(items: MutableList<Item>) {
        view?.populateRecomendedVenues(items.sortedBy { it.venue.location.distance }.reversed() as MutableList<Item>)

    }

    fun toggleSortBy(isDistance: Boolean) {
        isDistanceSort = isDistance
        if (isDistance) {
            spHelper.putDatatoSharedPref(true, Boolean::class.java, AppConstants.SORT_DISTANCE)
            spHelper.putDatatoSharedPref(false, Boolean::class.java, AppConstants.SORT_RATING)
            view?.enableSortByDistance()

        } else {
            spHelper.putDatatoSharedPref(false, Boolean::class.java, AppConstants.SORT_DISTANCE)
            spHelper.putDatatoSharedPref(true, Boolean::class.java, AppConstants.SORT_RATING)
            view?.enableSortByRating()
            isDistanceSort = false
        }
        toggleSortType(isAscendingSort)
    }

    fun toggleSortType(isAscending: Boolean) {
        isAscendingSort = isAscending
        if (isAscending) {
            spHelper.putDatatoSharedPref(true, Boolean::class.java, AppConstants.SORT_ASCENDING)
            spHelper.putDatatoSharedPref(false, Boolean::class.java, AppConstants.SORT_DESCENDING)
            view?.enableAscendingSort()
        } else {
            spHelper.putDatatoSharedPref(false, Boolean::class.java, AppConstants.SORT_ASCENDING)
            spHelper.putDatatoSharedPref(true, Boolean::class.java, AppConstants.SORT_DESCENDING)
            view?.enableDescendingSort()
        }
        sortList()
    }

    fun sortList() {
        if (items != null && items!!.size>0)
            if (isDistanceSort && isAscendingSort)
                sortByAscendingDistance(items!!)
            else if (isDistanceSort && !isAscendingSort)
                sortByDescendingDistance(items!!)
            else if (!isDistanceSort && isAscendingSort)
                sortByAscendingRating(items!!)
            else if (!isDistanceSort && !isAscendingSort)
                sortByDescendingRating(items!!)
    }


}