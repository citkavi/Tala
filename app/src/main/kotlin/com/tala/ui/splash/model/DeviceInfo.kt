package com.tala.ui.splash.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



/**
 * Created by kavi on 03/12/17.
 */


class DeviceInfo {

    @SerializedName("as")
    @Expose
    var `as`: String? = null
    @SerializedName("city")
    @Expose
    var city: String? = null
    @SerializedName("country")
    @Expose
    var country: String? = null
    @SerializedName("countryCode")
    @Expose
    var countryCode: String? = null
    @SerializedName("isp")
    @Expose
    var isp: String? = null
    @SerializedName("lat")
    @Expose
    var lat: Double? = null
    @SerializedName("lon")
    @Expose
    var lon: Double? = null
    @SerializedName("org")
    @Expose
    var org: String? = null
    @SerializedName("query")
    @Expose
    var query: String? = null
    @SerializedName("region")
    @Expose
    var region: String? = null
    @SerializedName("regionName")
    @Expose
    var regionName: String? = null
    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("timezone")
    @Expose
    var timezone: String? = null
    @SerializedName("zip")
    @Expose
    var zip: String? = null

}