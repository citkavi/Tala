package com.tala.ui.splash.di

import com.tala.base.di.ActivityScope
import com.tala.base.di.component.AppComponent
import com.tala.ui.splash.SplashActivity
import dagger.Component

/**
 * Created by kavi on 03/12/17.
 */

@ActivityScope
@Component(dependencies = arrayOf(AppComponent::class), modules = arrayOf(SplashModule::class))
interface SplashComponent {

    fun inject(splashActivity: SplashActivity)
}
