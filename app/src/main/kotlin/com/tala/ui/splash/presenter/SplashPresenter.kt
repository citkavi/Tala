package com.tala.ui.splash.presenter

import com.tala.base.AppConstants
import com.tala.base.api.EndPoints
import com.tala.base.di.mvp.BasePresenter
import com.tala.base.helper.SpHelper
import com.tala.ui.splash.model.DeviceInfo
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

/**
 * Created by kavi on 03/12/17.
 */


class SplashPresenter @Inject constructor(var api: EndPoints, var spHelper: SpHelper) : BasePresenter<SplashView>() {

    fun getDeviceInfo(){
        api.getDeviceInformation("http://ip-api.com/json").enqueue(object : Callback<DeviceInfo> {
            override fun onResponse(call: Call<DeviceInfo>, response: Response<DeviceInfo>) {

                updatePrefLatLng(response.body().lat!!, response.body().lon!!)
            }
            override fun onFailure(call: Call<DeviceInfo>, t: Throwable) {
                view?.onError()
            }
        })
    }

    fun updatePrefLatLng(lat:Double, lng:Double){
        spHelper.putDatatoSharedPref(lat.toString(), String::class.java , AppConstants.LATITUDE)
        spHelper.putDatatoSharedPref(lng.toString(), String::class.java , AppConstants.LONGITUDE)
        view?.navigateToListActivity()
    }
}