package com.tala.ui.splash

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.util.Log
import android.widget.Toast
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.location.*
import com.tala.base.BaseActivity
import com.tala.ui.home.PlaceListActivity
import com.tala.ui.splash.di.DaggerSplashComponent
import com.tala.ui.splash.di.SplashModule
import com.tala.ui.splash.presenter.SplashPresenter
import com.tala.ui.splash.presenter.SplashView
import tala.com.tala_kavi.R
import javax.inject.Inject


class SplashActivity : BaseActivity(), SplashView {


    @Inject
    lateinit var presenter: SplashPresenter

    /**
     * Constant used in the location settings dialog.
     */
    protected val REQUEST_CHECK_SETTINGS = 0x1

    val MY_PERMISSIONS_REQUEST_LOCATION = 1

    protected val TAG = "SplashActivity"


    override fun onActivityInject() {
        DaggerSplashComponent.builder().appComponent(getAppcomponent())
                .splashModule(SplashModule())
                .build()
                .inject(this)
        presenter.attachView(this)
    }

    private var mFusedLocationClient: FusedLocationProviderClient? = null


    override fun navigateToListActivity() {
        Toast.makeText(this, "Navigation to next Activity", Toast.LENGTH_LONG).show()
        startActivity(Intent(this, PlaceListActivity::class.java))
        finish()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_splash)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);


        displayLocationSettingsRequest(this)
    }


    private fun displayLocationSettingsRequest(context: Context) {
        val googleApiClient = GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build()
        googleApiClient.connect()

        val locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 10000
        locationRequest.fastestInterval = (10000 / 2).toLong()

        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        builder.setAlwaysShow(true)

        val result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build())
        result.setResultCallback(object : ResultCallback<LocationSettingsResult> {
            override fun onResult(result: LocationSettingsResult) {
                val status = result.status
                when (status.statusCode) {
                    LocationSettingsStatusCodes.SUCCESS -> checkLocationPermission()
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {

                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(context as Activity?, REQUEST_CHECK_SETTINGS)
                        } catch (e: IntentSender.SendIntentException) {
                            Log.i(TAG, "PendingIntent unable to execute request.")
                        }

                    }
                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.")
                }
            }
        })
    }


    private fun checkLocationPermission() {

        val permissionArray = arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)


        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) || ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                showRationaleDialog(R.string.permission_location_rationale)

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        permissionArray,
                        MY_PERMISSIONS_REQUEST_LOCATION);

            }
        } else
            fetchLastLocation()
    }


    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    fetchLastLocation()

                } else {

                    presenter.getDeviceInfo()

                }
                return
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        when (requestCode) {
        // Check for the integer request code originally supplied to startResolutionForResult().
            REQUEST_CHECK_SETTINGS -> when (resultCode) {
                Activity.RESULT_OK -> {
                    Log.i(TAG, "User agreed to make required location settings changes.")
                    checkLocationPermission()
                }
                Activity.RESULT_CANCELED -> presenter.getDeviceInfo()
            }
        }
    }


    @SuppressLint("MissingPermission")
    private fun fetchLastLocation() {
        mFusedLocationClient?.getLastLocation()?.addOnSuccessListener(this) { location ->
            // Got last known location. In some rare situations this can be null.
            if (location != null) {

                Toast.makeText(this, "" + location.latitude + "--- " + location.longitude, Toast.LENGTH_LONG).show()
                presenter.updatePrefLatLng(location.latitude, location.longitude)

            }
        }
    }


    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)


    }


    private fun showRationaleDialog(@StringRes messageResId: Int) {
        AlertDialog.Builder(this)
                .setPositiveButton(R.string.button_allow) { _, _ -> Toast.makeText(this, "", Toast.LENGTH_LONG) }
                .setNegativeButton(R.string.button_deny) { _, _ -> Toast.makeText(this, "", Toast.LENGTH_LONG) }
                .setCancelable(false)
                .setMessage(messageResId)
                .show()
    }


}
