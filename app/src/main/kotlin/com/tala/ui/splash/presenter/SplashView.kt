package com.tala.ui.splash.presenter

import com.tala.base.di.mvp.BaseView

/**
 * Created by kavi on 03/12/17.
 */


interface SplashView : BaseView {
    fun navigateToListActivity()

}
