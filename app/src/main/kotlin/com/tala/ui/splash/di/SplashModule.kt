package com.tala.ui.splash.di

import com.tala.base.api.EndPoints
import com.tala.base.di.ActivityScope
import com.tala.base.helper.SpHelper
import com.tala.ui.splash.presenter.SplashPresenter
import dagger.Module
import dagger.Provides

/**
 * Created by kavi on 03/12/17.
 */


@Module
class SplashModule {

    @Provides
    @ActivityScope
    internal fun providesHomePresenter(api: EndPoints, spHelper: SpHelper): SplashPresenter {
        return SplashPresenter(api,spHelper)
    }
}
