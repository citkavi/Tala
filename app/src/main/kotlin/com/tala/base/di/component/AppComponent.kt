package com.tala.base.di.component

import android.app.Application
import android.content.res.Resources
import com.google.gson.Gson
import com.tala.base.api.EndPoints
import com.tala.base.di.module.ApiModule
import com.tala.base.di.module.AppModule
import com.tala.base.di.module.OkHttpModule
import com.tala.base.di.module.RetrofitModule
import com.tala.base.helper.SpHelper
import dagger.Component
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by kavi on 02/12/17.
 */


@Singleton
@Component(modules = arrayOf(AppModule::class, RetrofitModule::class, ApiModule::class, OkHttpModule::class))
interface AppComponent {
    fun application(): Application
    fun gson(): Gson
    fun resources(): Resources
    fun retrofit(): Retrofit
    fun endpoints():EndPoints
    fun cache(): Cache
    fun client(): OkHttpClient
    fun loggingInterceptor(): HttpLoggingInterceptor
    fun spHelper(): SpHelper
}