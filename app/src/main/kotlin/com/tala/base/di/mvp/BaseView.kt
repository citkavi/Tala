package com.tala.base.di.mvp

/**
 * Created by kavi on 02/12/17.
 */


interface BaseView {
    fun onError()
    fun setPresenter(presenter: BasePresenter<*>)
}
