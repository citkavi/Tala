package com.tala.base.di.module

import com.tala.base.api.EndPoints
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by kavi on 02/12/17.
 */


@Module
class ApiModule {
    @Provides
    @Singleton
    fun providesEndpoints(retrofit: Retrofit): EndPoints = retrofit.create(EndPoints::class.java)
}