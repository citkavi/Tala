package com.tala.base.di

/**
 * Created by kavi on 03/12/17.
 */
import javax.inject.Scope

@Scope @Retention annotation class ActivityScope