package com.tala.base.di.mvp

/**
 * Created by kavi on 02/12/17.
 */


interface Presenter<V : BaseView> {

    fun attachView(view: V)

    fun detachView()
}
