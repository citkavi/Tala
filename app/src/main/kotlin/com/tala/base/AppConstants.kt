package com.tala.base

/**
 * Created by kavi on 03/12/17.
 */
class AppConstants {

    companion object {
        val LATITUDE ="latitude"

        val LONGITUDE ="longitude"

        val SORT_DISTANCE = "sortDistance"

        val SORT_RATING= "sortRating"


        val SORT_ASCENDING= "sortAscending"

        val SORT_DESCENDING= "sortDescending"
    }

}