package com.tala.base

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco
import com.tala.base.di.component.AppComponent
import com.tala.base.di.component.DaggerAppComponent
import com.tala.base.di.module.AppModule

/**
 * Created by kavi on 02/12/17.
 */


class TalaApplication: Application() {

    companion object{
        @JvmStatic lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        initDagger()
        Fresco.initialize(this)
    }

    private fun initDagger() {
        appComponent = DaggerAppComponent.builder().appModule(AppModule(this)).build()
    }

}