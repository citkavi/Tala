package com.tala.base.api

import com.tala.ui.home.details.model.VenueDetails
import com.tala.ui.home.model.ExploredPlaces
import com.tala.ui.splash.model.DeviceInfo
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.QueryMap
import retrofit2.http.Url


/**
 * Created by kavi on 02/12/17.
 */


interface EndPoints {

    @GET
    fun getDeviceInformation(@Url url: String): Call<DeviceInfo>

/*    @GET("venues/search")
    fun getPlacesList(@QueryMap queries: HashMap<String, Any>): Call<PlacesResponse>*/

    @GET("venues/explore")
    fun getExplorePlaces(@QueryMap queries: HashMap<String, Any>): Call<ExploredPlaces>


    @GET("venues/{id}")
    fun getPlacesDetails(@Path("id") id: String, @QueryMap queries: HashMap<String, Any>): Call<VenueDetails>


}