package com.tala.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.tala.base.di.component.AppComponent
import com.tala.base.di.mvp.BasePresenter
import com.tala.base.di.mvp.BaseView
import com.tala.base.event.DefaultEvent
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.jetbrains.anko.toast


/**
 * Created by kavi on 02/12/17.
 */


abstract class BaseActivity : AppCompatActivity(), BaseView {

    private var presenter: BasePresenter<*>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        onActivityInject()
    }

    protected abstract fun onActivityInject()

    fun getAppcomponent(): AppComponent = TalaApplication.appComponent

    override fun setPresenter(presenter: BasePresenter<*>) {
        this.presenter = presenter
    }

    override fun onError() {
        toast("Something went wrong")
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }


    @Subscribe
    fun defaultSubscribe(event: DefaultEvent){}


    override fun onDestroy() {
        super.onDestroy()
        presenter?.detachView()
        presenter = null
    }


}
